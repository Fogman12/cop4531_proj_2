Michael Fogarty
Dr. Kumar
Programming Assignment #2
Due Date: 11/23/2020
gitlab repo: https://gitlab.com/Fogman12/cop4531_proj_2

Description:
Program upsamples a 256x256 image to a 512x512 image using nearest neighbor. L1 metric came out to ~2 million. Unit Test test the nearest neighbor algorithm and there is a
code coverage of at least 50%.

Tar Submission Contents:
-upsample.cpp
-driver.cpp
-test2.cpp
-Makefile
-README.txt
-CMakeLists.txt

Compilation Instructions:

1.run "make"
2.run "./main sample1_input_image.txt output.txt" (creates new image file in "output.txt")  
3.run "make googletest" (installs googletest) **THIS STEP IS NOT NECESSARY IF YOU ALREADY HAVE GOOGLETEST INSTALLED**
4.run "make test" (creates and runs test case executable)
5.run "make coverage"

Makefile Commands:

"make" - will compile upsample.cpp and driver.cpp to create an executable called "main". To test the upsample.cpp type "./main input.txt" where input.txt is the input image
file. The output is in "newImage.txt".

"make googletest" - will install Google Test

"make test" - will compile test2.cpp which contains the unit test cases to test upsample.cpp and then it will output the test results. It is assumed Google Test is installed.
If you receive an error with something about gtest/gtest.h not found then please run "make googletest" to install Google Test then run "make test".

"make coverage" - will install the gcoverage utility via "python3 -m pip install git+https://github.com/gcovr/gcovr.git" (PLEASE NOTE, YOU DO NOT NEED TO RUN THIS COMMAND AS IT
IS ALREADY IN THE MAKE COMMAND) it will then compile the test2.cpp and run the executable and then create the html pages and display the coverage to the screen.
