#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>

using namespace std;
/***************************************************
*Function: fileToVector(string&)                   *
*Description: Takes in a string constant and output*
* a vector of strings. The function reads in the   *
*data and pushes it to a vector so that the data is*
* easy to read for the algorithms.                 *
****************************************************/
vector<string> fileToVector(string& file)
{

    ifstream source; //create stream
    source.open(file); //open the file

    vector<string> input;
    string theLine;
    stringstream buffer;

    while(getline(source, theLine))
    {

        input.push_back(theLine); //get each line from input file and push into a vector

    }

    return input; //return vector with file data

}


/***************************************************
*Function: vectorToFile(vector<vector<int>>)       *
*Description: Takes in a vector of vector integers *
* to push to the output file.                      *
****************************************************/
int vectorToFile(vector<vector<int>> outputVector, string& outFile)
{
    ofstream outputFile; //create outfile stream

    outputFile.open(outFile); //output in new file named newImage.txt

    for(int i = 0; i < outputVector.size(); i++) //loop through the rows in the matric
    {
        outputFile << outputVector[i][0]; //output first matrix value to output file
        for(int j = 1; j < outputVector[i].size(); j++) //loop through column in specific row
        {
        outputFile << "," << outputVector[i][j]; //output current matrix value to output file

        }
        outputFile << "\n";

        outputFile << outputVector[i][0];
        for(int j = 1; j < outputVector[i].size(); j++)
        {
        outputFile << "," << outputVector[i][j];

        }
        outputFile << "\n";
    }
    return 1; //return 1 on success

}



/***************************************************
*Function: nearestNeighbor(vector<string>)         *
*Description: Takes in a vector of strings and     *
*goes through the strings only taking the integers *
*and putting those integers into a integer matrix. *
*This outputs a vector of vector integers.         *
****************************************************/
vector<vector<int>> nearestNeighbor(vector<string> input)
{
    vector<string> numList;
    vector<vector<int>> numIntList;
    vector<int> intVect;

    for(int i = 0; i < input.size(); i++) //goes through each string in vector
    {
        stringstream ss(input[i]); //open stream for each line
        string insert;
        intVect.clear();
        numList.clear();

        while(ss.good()){
            string substr;
            getline(ss, substr, ','); //get the integer value(skips the comma)
            numList.push_back(substr); //pushes int into a vector
        }

        for(int j = 0; j<numList.size(); j++)
        {
            int num = atoi(numList.at(j).c_str());//converts char num into a integer
            intVect.push_back(num);
            intVect.push_back(num);
        }

        numIntList.push_back(intVect); //pushes the row of integers into the matrix
    }

    return numIntList; //return matrix

}





