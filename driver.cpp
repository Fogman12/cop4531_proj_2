#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>

#include "upsample.cpp"

using namespace std;

int main(int argc, char *argv[]){

    string fileName(argv[1]); //read in the file name and assign to variable "fileName"
    string outputFileName(argv[2]);

    vector<string> inputData = fileToVector(fileName); // pass fileName to function then put return data into "inputData"

    vector<vector<int>> finalData = nearestNeighbor(inputData); //Now pass the inputData that is in the correct format to the nearest neighbor algorithm

    int value = vectorToFile(finalData, outputFileName); // pass the finalData to the output file function

    return 0;
}



