main: driver.o
        g++ -o main driver.o

driver.o: driver.cpp
        g++ -std=c++11 -c driver.cpp

googletest:
        git clone https://github.com/google/googletest.git
        cp CMakeLists.txt googletest
        cd googletest
        mkdir ./googletest/build
        cd googletest && cd build && cmake .. && make
        cd ..
        cd ..

test:
        g++ -std=c++11 test2.cpp -L ./googletest/build/lib -I ./googletest/googletest/include -lgtest -lpthread
        ./a.out

coverage:
        python3 -m pip install git+https://github.com/gcovr/gcovr.git --user
        g++ -std=c++11 -fprofile-arcs -ftest-coverage test2.cpp -L ./googletest/build/lib/ -I ./googletest/googletest/include/ -lgtest -lpthread
        ./a.out
        ~/.local/bin/gcovr -r . --html>coverage.html
        ~/.local/bin/gcovr -r . --html --html-details -o coverage-complete.html
        ~/.local/bin/gcovr

clean:
        rm *.o main
